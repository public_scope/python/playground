#!/usr/bin/env python

import os
import time
import datetime
import calendar
from dateutil.relativedelta import relativedelta


def getPeople():
    peoples = {
        1: {
            'name': {
                'first': "Sandy",
                'last': "Brown",
                'middle': "Renee",
                'maiden': "",
                'confirmation': "",
                'secondary': ""
            },
            'age': {
                'dob': {
                    'month': 9,
                    'day': 18,
                    'year': 1963
                }
            },
        },
        2: {
            'name': {
                'first': "Kimberly",
                'last': "Schoenauer",
                'middle': "Marie",
                'maiden': "Ricker",
                'confirmation': "Zita",
                'secondary': "Annie"
            },
            'age': {
                'dob': {
                    'month': 3,
                    'day': 16,
                    'year': 1984
                }
            },
        }
    }
    return peoples


def ordinal(num):
    suffixes = {1: 'st', 2: 'nd', 3: 'rd'}
    num = int(num)
    if 10 <= num % 100 <= 20:
        suffix = 'th'
    else:
        suffix = suffixes.get(num % 10, 'th')
    return str(num) + suffix


def main():
    peoples = getPeople()
    today = datetime.date.today()

    for people in peoples:
        birthDate = datetime.date(
            today.year, peoples[people]["age"]["dob"]["month"], peoples[people]["age"]["dob"]["day"])
        birthMonth = datetime.date(peoples[people]["age"]["dob"]["year"], peoples[people]
                                   ["age"]["dob"]["month"], peoples[people]["age"]["dob"]["day"]).strftime('%B')
        birthYear = datetime.date(peoples[people]["age"]["dob"]["year"], peoples[people]
                                  ["age"]["dob"]["month"], peoples[people]["age"]["dob"]["day"]).strftime('%Y')
        birthDay = datetime.date(peoples[people]["age"]["dob"]["year"], peoples[people]
                                 ["age"]["dob"]["month"], peoples[people]["age"]["dob"]["day"]).strftime('%d')
        birthWeekDay = datetime.date(peoples[people]["age"]["dob"]["year"], peoples[people]
                                     ["age"]["dob"]["month"], peoples[people]["age"]["dob"]["day"]).strftime('%A')
        numOfDays = birthDate - today

        if numOfDays.days < 0:
            numOfDaysThisYear = datetime.date(today.year, 12, 31) - today
            print(numOfDaysThisYear.days)

            nextBirthDay = datetime.date(today.year, peoples[people]["age"]["dob"]["month"], peoples[people]["age"]["dob"]["day"]) + relativedelta(years=+1)
            numOfDaysfromBeginning = (nextBirthDay - (datetime.date(today.year, 1, 1) + relativedelta(years=+1)))
            print(numOfDaysfromBeginning)

            daysToBirthday = numOfDaysThisYear.days + numOfDaysfromBeginning.days + 1
        else:
            daysToBirthday = numOfDays.days

        print(
            "Hello, " + peoples[people]["name"]["first"] +
            " " + peoples[people]["name"]["last"] + "\n"
            "Your birthday is in " + birthMonth + " "
            "and is on the " + ordinal(birthDay) + ". " +
            "In " + birthYear + " that was on a " + birthWeekDay + " " +
            "It is " + str(daysToBirthday) + " days away\n"
        )


main()
